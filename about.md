---
layout: page
title: About
permalink: /about/
---


# Alejandro Pacheco 

Con de 20 años de experiencia en la Industría. Ha trabajando para Microsoft, Oracle y Startups como Pivotal [VMware Tanzu, hoy].

Hoy lidera junto con Orión una iniciativa para promover DevSecOps y el desarrollo de aplicaciones nativas de Nube.


This is the base Jekyll theme. You can find out more info about customizing your Jekyll theme, as well as basic Jekyll usage documentation at [jekyllrb.com](https://jekyllrb.com/)

You can find the source code for the Jekyll new theme at:
{% include icon-github.html username="jekyll" %} /
[minima](https://github.com/jekyll/minima)

You can find the source code for Jekyll at
{% include icon-github.html username="jekyll" %} /
[jekyll](https://github.com/jekyll/jekyll)
