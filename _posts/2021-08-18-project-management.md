---
layout: post
title:  "Project Management"
date:   2021-08-18 21:00:00 -0500
categories: 10-min videos
video: https://www.youtube.com/embed/YzlI2z_bGAo 
---

{% capture video_link %}
{{ page.video }}
{% endcapture %}

{% include embed-youtube.html linktovideo=video_link %}
